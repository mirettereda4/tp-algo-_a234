#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "a234.h"
#include "file.h"
#include"pile.h"


#define max(a,b) ((a)>(b)?(a):(b))
int Est_Feuille(Arbre234 a) {
  if ((a->fils[0]==NULL || a->fils[0]->t==0) && (a->fils[1]==NULL || a->fils[1]->t==0) && (a->fils[2]==NULL || a->fils[2]->t==0) && (a->fils[3]==NULL || a->fils[3]->t==0)) {
    return 1;
  }
  return 0;
}
int hauteur (Arbre234 a)
{
  int h0, h1, h2, h3 ;
  
  if (a == NULL)
    return 0 ;

  if (a->t == 0)
    return 0 ;

  h0 = hauteur (a->fils [0]) ;
  h1 = hauteur (a->fils [1]) ;
  h2 = hauteur (a->fils [2]) ;
  h3 = hauteur (a->fils [3]) ;

  return 1 + max (max (h0,h1),max (h2,h3)) ;
} 

int NombreCles (Arbre234 a)
{
 
  if (a==NULL)
    return 0;
  
  if (a->t==0){
    
      return 0;
  }
  if (a->t==2){
      return 1+ NombreCles(a->fils[1])+
      NombreCles(a->fils[2]);
      }
  if (a->t==3){
      return 2+  NombreCles(a->fils[0])+NombreCles(a->fils[1])+
      NombreCles(a->fils[2]);
      }
  if (a->t==4) {
      return 3+ NombreCles(a->fils[0])+NombreCles(a->fils[1])+
      NombreCles(a->fils[2])+ NombreCles(a->fils[3]);
      
  } 

  
  return 0;
  
}

int CleMax (Arbre234 a)
{
  if (a->t==4){
  	if (a->fils[0]->t==0 && a->fils[1]->t==0&&a->fils[2]->t==0&&a->fils[3]->t==0){
  	return a ->cles[2];
  	}
  	else {
  		return CleMax(a->fils[3]);
  	}
  }
  if (a->t==3){
  	if (a->fils[0]->t==0 && a->fils[1]->t==0&&a->fils[2]->t==0){
  		return a ->cles[1];
  		}
  		
  	else {
  		return CleMax(a->fils[2]);
  	}	
  
  }
  if (a->t==2){
  if ( a->fils[1]->t==0&&a->fils[2]->t==0){
  		return a ->cles[1];
  		}
  		
  	else {
  		return CleMax(a->fils[2]);
  	}	
  }
  
  return 0 ;
}

int CleMin (Arbre234 a)
{
  if (a->t==4){
  	if (a->fils[0]->t==0 && a->fils[1]->t==0&&a->fils[2]->t==0&&a->fils[3]->t==0){
  	return a ->cles[0];
  	}
  	else {
  		return CleMin(a->fils[0]);
  	}
  }
  if (a->t==3){
  	if (a->fils[0]->t==0 && a->fils[1]->t==0&&a->fils[2]->t==0){
  		return a ->cles[0];
  		}
  		
  	else {
  		return CleMin(a->fils[0]);
  	}	
  
  }
  if (a->t==2){
  if ( a->fils[1]->t==0&&a->fils[2]->t==0){
  		return a ->cles[1];
  		}
  		
  	else {
  		return CleMin(a->fils[1]);
  	}	
  }
  

  return 0 ;
}int position (int x, Arbre234 a){
	//int pos;
	if (a->t==2){
		if (x<=a->cles[1]){
		
			return 1;
		}
		else {
		
			return 2;
		}
			
	}
	if (a->t==3){
		if (x<=a->cles[0]){
		
			return 0;
		}
		if (x<=a->cles[1]){
		
			return 1;
		}
		
		return 2;	
	}
	if (a->t==4){
		if (x<=a->cles[0]){
		
			return 0;
		}
		if (x<=a->cles[1]){
		
			return 1;
		}
		if (x<=a->cles[2]){
		
			return 2;
		}
		return 3;	
	}
	
	/*for (pos=0;pos<a->t&&!trouve;pos++){
		trouve=(x<=a->cles[pos]);
	}
	return pos;*/
	return -1;
}	

Arbre234 RechercherCle (Arbre234 a, int cle)
{
	//printf("type%d\n",a->t);
	int pos;
	if (a==NULL){
		return NULL;
	}
	else {
		pos=position(cle,a);
		
		pos=position(cle,a);
		
		if (pos!=-1){
			if((a->t==2&&(pos==1))||
			   (a->t==3&&(pos==0||pos==1))||
			   (a->t==4&&(pos==0||pos==1||pos==2))){
			
			if (cle==a->cles[pos]){
			
				return a ;
			}
			else {
				RechercherCle(a->fils[pos],cle);
				
			}
			}
			else {
				RechercherCle(a->fils[pos],cle);
				
			}
		}
		else{
		
		return NULL;
		}
		
		
	}
	
}


void AnalyseStructureArbre (Arbre234 a, int *feuilles, int *noeud2, int *noeud3, int *noeud4)
{
	if (a->t==0){
  	*feuilles=*feuilles+1;
  	return ;
  }
  if (a->t==2) {
	*(noeud2)=*(noeud2)+1;
	AnalyseStructureArbre(a->fils[1], feuilles, noeud2, noeud3, noeud4);
	AnalyseStructureArbre(a->fils[2], feuilles, noeud2, noeud3, noeud4);
  }

  if (a->t==3) {
	*(noeud3)=*(noeud3)+1;
	AnalyseStructureArbre(a->fils[0], feuilles, noeud2, noeud3, noeud4);
	AnalyseStructureArbre(a->fils[1], feuilles, noeud2, noeud3, noeud4);
	AnalyseStructureArbre(a->fils[2], feuilles, noeud2, noeud3, noeud4);
  }

  if (a->t==4) {
	*(noeud4)=*(noeud4)+1;
	AnalyseStructureArbre(a->fils[0], feuilles, noeud2, noeud3, noeud4);
	AnalyseStructureArbre(a->fils[1], feuilles, noeud2, noeud3, noeud4);
	AnalyseStructureArbre(a->fils[2], feuilles, noeud2, noeud3, noeud4);
	AnalyseStructureArbre(a->fils[3], feuilles, noeud2, noeud3, noeud4);
  }
}




int somme_cles (Arbre234 a) 
{
   int sum = 0 ;
   
   if (a == NULL)
     return 0 ;
     
   else
   {  
   if (a->t == 2)
     sum = a->cles[1] ;
     
   if (a->t == 3)
     sum = a->cles[0] + a->cles[1] ;  
     
   if (a->t == 4)
     sum = a->cles[0] + a->cles[1] + a->cles[2] ;  
   }
      
   return sum ;   
}


Arbre234 noeud_max (Arbre234 a)
{
   /*
    Retourne le noeud avec la somme maximale des cles internes
   */
   
   Arbre234 n0, n1, n2, n3, n_max ;
   int s_max ;
   
   if (a == NULL || a->t == 0 || a->fils[1]->t == 0)
     return a ;
   
   n0 = noeud_max (a->fils [0]) ;
   n1 = noeud_max (a->fils [1]) ;
   n2 = noeud_max (a->fils [2]) ;
   n3 = noeud_max (a->fils [3]) ;
   
   Arbre234 noeud_tab[4] = {n0, n1, n2, n3} ;
   
   int sum_tab[4] = {somme_cles (n0), somme_cles (n1), somme_cles (n2), somme_cles (n3)} ;
   
   s_max = somme_cles(a) ;
   n_max = a ;
   
   for (int i = 0 ; i < 4 ; i++)
   {
      if (sum_tab[i] > s_max)
      {
         s_max = sum_tab[i] ;
         n_max = noeud_tab[i] ;
      }
   }
   
   return n_max ;
}

void Afficher_Cles_Largeur (Arbre234 a)
{

  pfile_t file=creer_file();
  if (a!=NULL){
  	enfiler(file,a);
  
  }
  Arbre234 noeud=NULL;
  while (!file_vide(file)){
 	noeud=defiler(file);
	 		 if (noeud->t==2) {
				printf(" %d ", noeud->cles[1]);
	 			enfiler(file, noeud->fils[1]);
				enfiler(file, noeud->fils[2]);
	  		}

	  		if (noeud->t==3) {
				printf(" %d ",noeud ->cles[0]);
				printf(" %d ", noeud->cles[1]);
				enfiler(file, noeud->fils[0]);
	 			enfiler(file, noeud->fils[1]);
				enfiler(file, noeud->fils[2]);
	  		}

			  if (noeud->t==4) {
				printf(" %d ", noeud->cles[0]);
				printf(" %d ", noeud->cles[1]);
				printf(" %d ", noeud->cles[2]);
				enfiler(file, noeud->fils[0]);
	 			enfiler(file, noeud->fils[1]);
				enfiler(file, noeud->fils[2]);
				enfiler(file, noeud->fils[3]);
	  		}
	  }
	  detruire_file(file);		
 
  
}

void Affichage_Cles_Triees_Recursive (Arbre234 a)
{
  if (a!=NULL) {

  		if (a->t==2) {
			Affichage_Cles_Triees_Recursive(a->fils[1]);
			printf(" %d ", a->cles[1]);
 			Affichage_Cles_Triees_Recursive(a->fils[2]);
  		}

  		if (a->t==3) {
			Affichage_Cles_Triees_Recursive(a->fils[0]);
			printf(" %d ", a->cles[0]);
			Affichage_Cles_Triees_Recursive(a->fils[1]);
			printf(" %d ", a->cles[1]);
			Affichage_Cles_Triees_Recursive(a->fils[2]);
  		}

		  if (a->t==4) {
			Affichage_Cles_Triees_Recursive(a->fils[0]);
			printf(" %d ", a->cles[0]);
			Affichage_Cles_Triees_Recursive(a->fils[1]);
			printf(" %d ", a->cles[1]);
			Affichage_Cles_Triees_Recursive(a->fils[2]);
			printf(" %d ", a->cles[2]);
			Affichage_Cles_Triees_Recursive(a->fils[3]);
  		}
	}else{printf("error");}
     
}


void Affichage_Cles_Triees_NonRecursive (Arbre234 a)
{
	ppile_t pile=creer_pile();
	if (a!=NULL){
		empiler(pile,a);
	}
	while (!pile_vide(pile)){
		Arbre234 noeud=depiler(pile);
		if (noeud!=NULL){
			if (noeud->t==0&&noeud->cles[0]!=0){
				printf(" %d ",noeud->cles[0]);
			}
		
		
			if (noeud->t==2) {
				Arbre234 noeud1=(Arbre234)malloc(sizeof(pnoeud234));
				noeud1->t=0;
				
				noeud1->cles[0]=noeud->cles[1];
				empiler(pile,noeud->fils[2]);
				empiler(pile,noeud1);
	 			empiler(pile,noeud->fils[1]);
	  		}

	  		if (noeud->t==3) {
	  			Arbre234 noeud1=(Arbre234)malloc(sizeof(pnoeud234));
	  			Arbre234 noeud2=(Arbre234)malloc(sizeof(pnoeud234));
	  			noeud1->t=0;
	  			noeud2->t=0;
	  			noeud1->cles[0]=noeud->cles[1];
	  			noeud2->cles[0]=noeud->cles[0];
	                       empiler(pile,noeud->fils[2]);
				empiler(pile,noeud1);
				empiler(pile,noeud->fils[1]);
				empiler(pile,noeud2);
				empiler(pile,noeud->fils[0]);
	  		}

			  if (noeud->t==4) {
			  	Arbre234 noeud1=(Arbre234)malloc(sizeof(pnoeud234));
	  			Arbre234 noeud2=(Arbre234)malloc(sizeof(pnoeud234));
	  			Arbre234 noeud3=(Arbre234)malloc(sizeof(pnoeud234));
	  			noeud1->t=0;
	  			noeud2->t=0;
	  			noeud3->t=0;
	  			noeud1->cles[0]=noeud->cles[2];
	  			noeud2->cles[0]=noeud->cles[1];
	  			noeud3->cles[0]=noeud->cles[0];
	  			
				empiler(pile,noeud->fils[3]);
				empiler(pile,noeud1);
				empiler(pile,noeud->fils[2]);
				empiler(pile,noeud2);
				empiler(pile,noeud->fils[1]);
				empiler(pile,noeud3);
				empiler(pile,noeud->fils[0]);
				
	  		}
	  	}

       }
	printf("\n");
}

void SwitchKids(Arbre234 parent, int mauvaisFils, int bonFils) {
  if (mauvaisFils < bonFils) { 
    //Donc celui est seul est à droite
    //Fils gauche transformation en 3-noeud
    (parent->fils[mauvaisFils])->t=3;
    (parent->fils[mauvaisFils])->fils[0]=(parent->fils[mauvaisFils])->fils[1];
    (parent->fils[mauvaisFils])->fils[1]=(parent->fils[mauvaisFils])->fils[2];
    (parent->fils[mauvaisFils])->fils[2]=(parent->fils[bonFils])->fils[0];
    (parent->fils[mauvaisFils])->cles[0]=(parent->fils[mauvaisFils])->cles[1];
    (parent->fils[mauvaisFils])->cles[1]=parent->cles[mauvaisFils];
    //Fin transformation
    //Modifies la cles du parent
    parent->cles[mauvaisFils]=(parent->fils[bonFils])->cles[0];
    //Parent bien modifié
    //Modification du fils droit
    (parent->fils[bonFils])->t=(parent->fils[bonFils])->t-1;
    if ((parent->fils[bonFils])->t==3) { 
    // Réarrangement des clés
      (parent->fils[bonFils])->cles[0]=(parent->fils[bonFils])->cles[1];
      (parent->fils[bonFils])->cles[1]=(parent->fils[bonFils])->cles[2];
    }

  } else {
    (parent->fils[mauvaisFils])->t=3;
    if ((parent->fils[bonFils])->t==3) {
      (parent->fils[mauvaisFils])->fils[0]=(parent->fils[bonFils])->fils[2];
    } else {
      (parent->fils[mauvaisFils])->fils[0]=(parent->fils[bonFils])->fils[3];
    }
    (parent->fils[mauvaisFils])->cles[0]=parent->cles[1];
    //Fin transformation
    //Modifies la cles du parent
    if ((parent->fils[bonFils])->t==3) {
      parent->cles[1]=(parent->fils[bonFils])->cles[1];
    } else {
      parent->cles[1]=(parent->fils[bonFils])->cles[2];
    }
    //Parent bien modifié
    //Modification du fils gauche
    (parent->fils[bonFils])->t=((parent->fils[bonFils])->t)-1;
    if ((parent->fils[bonFils])->t==2) { // Réarrangement des clés
      (parent->fils[bonFils])->cles[1]=(parent->fils[bonFils])->cles[0];
    }
  }
}

void transformation2to4(Arbre234 parent) {
  parent->t=4;
  //On ajoute les keys le 1 est deja rempli
  parent->cles[0]=(parent->fils[1])->cles[1];
  parent->cles[2]=(parent->fils[2])->cles[1];

  //On s'occupe du fils gauche (2 fils)
  parent->fils[0]=(parent->fils[1])->fils[1];
  parent->fils[1]=(parent->fils[1])->fils[2];

  //Puis du fils droit (2 fils)
  parent->fils[3]=(parent->fils[2])->fils[2];
  parent->fils[2]=(parent->fils[2])->fils[1];
}

void transformation34(Arbre234 parent, int directionFils, int autreFils, int indiceCleParent) {
  if (directionFils<autreFils) {
    // DES CLES DU FILS DANS LEQUEL ON VA ALLER
    (parent->fils[directionFils])->cles[0]=(parent->fils[directionFils])->cles[1];
    (parent->fils[directionFils])->cles[1]=(parent->cles[indiceCleParent]);
    (parent->fils[directionFils])->cles[2]=(parent->fils[autreFils])->cles[1];
    // DES CLES DU PARENT
     if(parent->t!=4) {
      if (indiceCleParent==1) {
        parent->cles[1]=parent->cles[0];
        parent->fils[2]=parent->fils[directionFils];
        parent->fils[1]=parent->fils[0];
      } else {
        parent->fils[1]=parent->fils[directionFils];
      }
    }
  } else {
    (parent->fils[directionFils])->cles[2]=(parent->fils[directionFils])->cles[1];
    (parent->fils[directionFils])->cles[1]=(parent->cles[indiceCleParent]);
    (parent->fils[directionFils])->cles[0]=(parent->fils[autreFils])->cles[1];

    if(parent->t==4) {
      parent->fils[2]=parent->fils[directionFils];
    } else {
      parent->cles[1]=parent->cles[0];
      parent->fils[2]=parent->fils[directionFils];
      parent->fils[1]=parent->fils[0];
    }
  }

  parent->fils[directionFils]->t=4;
  parent->t=(parent->t)-1;

}

void choixTransformation(Arbre234 a, int indiceFilsDirection) {
//sert à choisir quel type de transformation à faire 
  if (a->t==2 && (a->fils[2])->t==2 && (a->fils[1])->t==2) {
    transformation2to4(a);
  } else {
  if (a->t==2) {
    if (indiceFilsDirection==1) {
        if (a->fils[indiceFilsDirection+1]->t!=2) {
          SwitchKids(a, indiceFilsDirection, indiceFilsDirection+1);
        } else {
          transformation34(a, indiceFilsDirection, indiceFilsDirection+1, indiceFilsDirection);
        }
      } else {
        if (a->fils[indiceFilsDirection-1]->t!=2) {
          SwitchKids(a, indiceFilsDirection, indiceFilsDirection-1);
        }   else {
          transformation34(a, indiceFilsDirection, indiceFilsDirection-1, indiceFilsDirection-1);
        }
      }
  } else {
    if (indiceFilsDirection<(a->t)-1) {
        if (a->fils[indiceFilsDirection+1]->t!=2) {
          SwitchKids(a, indiceFilsDirection, indiceFilsDirection+1);
        } else {
          transformation34(a, indiceFilsDirection, indiceFilsDirection+1, indiceFilsDirection);
        }
      } else {
        if (a->fils[indiceFilsDirection-1]->t!=2) {
          SwitchKids(a, indiceFilsDirection, indiceFilsDirection-1);
        }   else {
          transformation34(a, indiceFilsDirection, indiceFilsDirection-1, indiceFilsDirection-1);
        }
      }
    }
  }
}


void Detruire_Feuille(Arbre234 parent, Arbre234 a, int cle, int indice_fils) {
  //FIRST CASE SI FEUILLE
  if (parent==NULL && Est_Feuille(a)) {
    if (a->t==2) {
      a->t=0;
    } else if (a->t==3) {
      a->t=2;
      if (a->cles[1]==cle) {
        a->cles[1]=a->cles[0];
      }
    } else if (a->t==4) {
      a->t=3;
      if (a->cles[0]==cle) {
        a->cles[0]=a->cles[1];
        a->cles[1]=a->cles[2];
      } else if (a->cles[1]==cle) {
        a->cles[1]=a->cles[2];
      }
    }
  } else {
    if (a->t>2 && Est_Feuille(a)) {
      pnoeud234 newNode=malloc(sizeof(noeud234));
      newNode->t=(a->t)-1;
      if (newNode->t==2) {
        for (int i=0;i<=1;i++) {
          if (a->cles[i]!=cle) newNode->cles[1]=a->cles[i];
        }
      }
      if (newNode->t==3) {
        int j=0;
        for (int i=0;i<=2;i++) {
          if (a->cles[i]!=cle) {
            newNode->cles[j]=a->cles[i];
            j++;
          }
        }
      }
      parent->fils[indice_fils]=newNode;
    }
  }
}

void creerArbreFusion(Arbre234 * newArbre, Arbre234 abrGauche, Arbre234 abrDroite) {
// commentaire à enlever avant de déposer le travail 
  pfile_t f = creer_file();
  enfiler(f, abrGauche);
  enfiler(f, abrDroite);
  while(!file_vide(f)){
    Arbre234 noeud = defiler(f);
    if(noeud->t == 2){
      ajouter_cle (newArbre, noeud->cles[1], 0, NULL) ;

      Arbre234 fils1 = noeud->fils[1];
      Arbre234 fils2 = noeud->fils[2];
      if(fils1->cles[0]!=0 || fils1->cles[1]!=0 || fils1->cles[2]!=0  || fils1->cles[3]!=0){
        enfiler(f, fils1);
      }
      if(fils2->cles[0]!=0 || fils2->cles[1]!=0 || fils2->cles[2]!=0  || fils2->cles[3]!=0){
        enfiler(f, fils2);
      }
    }
    if(noeud->t == 3){
      ajouter_cle (newArbre, noeud->cles[1], 0, NULL) ;
      ajouter_cle (newArbre, noeud->cles[0], 0, NULL) ;

      Arbre234 fils1 = noeud->fils[0];
      Arbre234 fils2 = noeud->fils[1];
      Arbre234 fils3 = noeud->fils[2];

      if(fils1->cles[0]!=0 || fils1->cles[1]!=0 || fils1->cles[2]!=0  || fils1->cles[3]!=0){
        enfiler(f, fils1);
      }
      if(fils2->cles[0]!=0 || fils2->cles[1]!=0 || fils2->cles[2]!=0  || fils2->cles[3]!=0){
        enfiler(f, fils2);
      }
      if(fils3->cles[0]!=0 || fils3->cles[1]!=0 || fils3->cles[2]!=0  || fils3->cles[3]!=0){
        enfiler(f, fils3);
      }
    }
    if(noeud->t == 4){
      ajouter_cle (newArbre, noeud->cles[2], 0, NULL) ;
      ajouter_cle (newArbre, noeud->cles[1], 0, NULL) ;
      ajouter_cle (newArbre, noeud->cles[0], 0, NULL) ;

      Arbre234 fils1 = noeud->fils[0];
      Arbre234 fils2 = noeud->fils[1];
      Arbre234 fils3 = noeud->fils[2];
      Arbre234 fils4 = noeud->fils[3];
      if(fils1->cles[0]!=0 || fils1->cles[1]!=0 || fils1->cles[2]!=0  || fils1->cles[3]!=0){
        enfiler(f, fils1);
      }
      if(fils2->cles[0]!=0 || fils2->cles[1]!=0 || fils2->cles[2]!=0  || fils2->cles[3]!=0){
        enfiler(f, fils2);
      }
      if(fils3->cles[0]!=0 || fils3->cles[1]!=0 || fils3->cles[2]!=0  || fils3->cles[3]!=0){
        enfiler(f, fils3);
      }
      if(fils4->cles[0]!=0 || fils4->cles[1]!=0 || fils4->cles[2]!=0  || fils4->cles[3]!=0){
        enfiler(f, fils4);
      }
    }
  }
  detruire_file(f);
}


void Detruire_noeud(Arbre234 parent, int cle, int indice_parent) {
//sert au réequilibrage 
  if (!Est_Feuille(parent) && parent->t==2 && ((parent->fils[1])->t==2) && ((parent->fils[2])->t==2)) {
    transformation2to4(parent);
  }
  if ((parent->fils[indice_parent])->t==2 && (parent->fils[indice_parent+1])->t==2) {
    Arbre234 newFils = NULL;
    creerArbreFusion(&newFils, parent->fils[indice_parent], parent->fils[indice_parent+1]);
    if (parent->t==3) {
      parent->fils[indice_parent+1]=newFils;
      if (indice_parent==1) {
        parent->cles[indice_parent]=parent->cles[0];
      }
    } else {
      if(indice_parent==0) {
        parent->fils[0]=newFils;
        parent->fils[1]=parent->fils[2];
        parent->fils[2]=parent->fils[3];
        parent->cles[0]=parent->cles[1];
        parent->cles[1]=parent->cles[2];
      } else if (indice_parent==1) {
        parent->fils[1]=newFils;
        parent->fils[2]=parent->fils[3];
        parent->cles[1]=parent->cles[2];
      } else {
        parent->fils[2]=newFils;
      }
    }
    parent->t=parent->t - 1;
  } else {
    if((parent->fils[indice_parent])->t!=2) {
      if ((parent->fils[indice_parent])->t==3) {
        parent->cles[indice_parent]=(parent->fils[indice_parent])->cles[1];
      } else {
        parent->cles[indice_parent]=(parent->fils[indice_parent])->cles[2];
      }
      //pour supprimer clé switché pendant un réequilibrage 
      Arbre234 newArbre = NULL;
      if ((parent->fils[indice_parent])->t==3) {
        (parent->fils[indice_parent])->fils[3]=(parent->fils[indice_parent])->fils[2];
        (parent->fils[indice_parent])->fils[2]=(parent->fils[indice_parent])->fils[1];
        (parent->fils[indice_parent])->fils[1]=(parent->fils[indice_parent])->fils[0];
        (parent->fils[indice_parent])->cles[1]=(parent->fils[indice_parent])->cles[0];
      }
      (parent->fils[indice_parent])->t=(parent->fils[indice_parent])->t-1;
      creerArbreFusion(&newArbre, (parent->fils[indice_parent]), (parent->fils[indice_parent])->fils[3]);
      (parent->fils[indice_parent])=newArbre;
    }
  }
}

void Detruire_Cle_Bis (Arbre234 a, Arbre234 previous, int indice_fils, int cle) {
  int Est_Detruit=-1;
  if (a!=NULL) {

    if(a->t==2) {
      if (a->cles[1]==cle) {
        if (Est_Feuille(a)) {
          Detruire_Feuille(previous, a , cle, indice_fils);
        } else {
          Detruire_noeud(a, cle, 1);
        }
        Est_Detruit=1;
      }
    } else if (a->t==3) {
      if (a->cles[1]==cle || a->cles[0]==cle) {
        if (Est_Feuille(a)) {
          Detruire_Feuille(previous, a , cle, indice_fils);
        } else {
          if (a->cles[1]==cle) {
            Detruire_noeud(a, cle, 1);
          } else {
            Detruire_noeud(a, cle, 0);
          }
        }
        Est_Detruit=1;
      }
    } else {
      if (a->cles[2]==cle || a->cles[1]==cle || a->cles[0]==cle) {
        if (Est_Feuille(a)) {
          Detruire_Feuille(previous, a , cle, indice_fils);
        } else {
          if (a->cles[1]==cle) {
            Detruire_noeud(a, cle, 1);
          } else if (a->cles[2]==cle) {
            Detruire_noeud(a, cle, 2);
          } else {
            Detruire_noeud(a, cle, 0);
          }
        }
        Est_Detruit=1;
      }
    }



    if (Est_Detruit!=1) {
      if (a->t==2) {
        if (cle > a->cles[1]) {
          if ((a->fils[2])->t==2) {
            choixTransformation(a, 2);
            Detruire_Cle_Bis(a, previous, indice_fils, cle);
          } else {
            Detruire_Cle_Bis(a->fils[2], a , 2, cle);
          }
        } else {
          if ((a->fils[1])->t==2) {
            choixTransformation(a, 1);
            Detruire_Cle_Bis(a, previous, indice_fils, cle);
          } else {
            Detruire_Cle_Bis(a->fils[1], a, 1, cle);
          }
        }
      } else if (a->t==3) {
        if (cle > a->cles[1]) {
          if ((a->fils[2])->t==2) {
            choixTransformation(a, 2);
            Detruire_Cle_Bis(a, previous, indice_fils, cle);
          } else {
            Detruire_Cle_Bis(a->fils[2], a, 2, cle);
          }
        } else if (cle > a->cles[0]) {
          if ((a->fils[1])->t==2) {
            choixTransformation(a, 1);
            Detruire_Cle_Bis(a, previous, indice_fils, cle);
          } else {
            Detruire_Cle_Bis(a->fils[1], a, 1, cle);
          }
        } else {
          if ((a->fils[0])->t==2) {
            choixTransformation(a, 0);
            Detruire_Cle_Bis(a, previous, indice_fils, cle);
          } else {
            Detruire_Cle_Bis(a->fils[0], a, 0, cle);
          }
        }
      } 
    }
  }
}

void Detruire_Cle(Arbre234 a, int cle) {
// bis c'est juste pour ne pas changer la signature de la fonction donnée par le prof 
  Detruire_Cle_Bis(a, NULL, -1, cle);
}


int main (int argc, char **argv)
{
  Arbre234 a ;
  Arbre234 noeud ; 

  if (argc != 2)
    {
      fprintf (stderr, "il manque le parametre nom de fichier\n") ;
      exit (-1) ;
    }

  a = lire_arbre (argv [1]) ;

  printf ("==== Afficher arbre ====\n") ;
  
  afficher_arbre (a, 0) ;
  printf("\n---------------");
  printf ("\n====  Test de la fonction hauteur====\n") ;
  int x=hauteur(a);
  printf("\nla hauteur de l'arbre =%d",x);
  printf("\n---------------");
  printf ("\n====  Test de la fonction NombreCles====\n") ;
  x=NombreCles(a);
  printf("\nle nombre de cles de l'arbre est %d",x);
  printf("\n");
  printf("-----------------\n");
  printf ("\n====  Test de la fonction clé Max====\n") ;
  x=CleMax(a);
  printf("la cle maximale =%d\n ",x);
  
  printf("-----------------\n");
  printf ("\n====  Test de la fonction cle min ====\n") ;
  x=CleMin(a);
  printf("la cle minimale =%d\n ",x);
  
  printf("-----------------\n");
   printf ("\n====  Test de la fonction rechercher clé ====\n") ;
  printf("recherche de la cle 400 dans l'arbre 16\n");
  Arbre234 b=RechercherCle(a,400);
  if (b!=NULL)
  	printf("cle trouvée\n");
  else 	
  	printf("cle non trouvée\n");
  printf("recherche de la cle 285 dans l'arbre 16\n");
   b=RechercherCle(a,285);
  if (b!=NULL)
  	printf("cle trouvée\n");
  else 	
  	printf("cle non trouvée\n");	
  printf("-----------------\n");
  printf ("\n====  Test de la fonction AnalyseStructureArbre  ====\n") ;
  int * feuilles=malloc(sizeof(int));
  int * noeud2=malloc(sizeof(int));
  int * noeud3=malloc(sizeof(int));
  int * noeud4=malloc(sizeof(int));
  *feuilles=0;
  *noeud2=0;
  *noeud3=0;
  *noeud4=0;
  AnalyseStructureArbre(a, feuilles, noeud2, noeud3, noeud4);
  printf ("Nombre de feuilles : %d\n", *feuilles);
  printf ("Nombre de noeud2 : %d\n", *noeud2);
  printf ("Nombre de noeud3 : %d\n", *noeud3);
  printf ("Nombre de noeud4 : %d\n", *noeud4);
	
  free(feuilles);
  free(noeud2);
  free(noeud3);
  free(noeud4);

  
  noeud = noeud_max (a) ;
  
  printf ("\n====  Test de la fonction noeud_max  ====\n") ;
  afficher_arbre (noeud, 0) ;
  printf("-----------------------------------\n");
  
  printf ("\n====  Test de la fonction afficher cles largeur ====\n") ;
  
  Afficher_Cles_Largeur(a);
  printf("\n");
    printf("-----------------------------------\n");
  printf ("==== Affiche clé trié en récursive ====\n");
  Affichage_Cles_Triees_Recursive(a);
  printf("\n");
    printf("-----------------------------------\n");
 printf ("\n====  Test de la fonction non recursive   ====\n") ;
  
  Affichage_Cles_Triees_NonRecursive(a);
  printf("-----------------------------------\n");
   printf ("\n====  Test de la fonction  detruire cle  ====\n") ;
  Detruire_Cle(a,80);
  afficher_arbre (a, 0) ;
}

